package main

import (
	"fmt"
	"strconv"
)

const (
	YES = "TAK"
	NO  = "NIE"
)

type Select struct {
	question string
	options  []string
	callback func(value string)
}

func NewSelect(question string) *Select {

	s := new(Select)
	s.question = question
	s.options = []string{YES, NO}

	return s
}

// Set options
func (s *Select) SetOptions(options []string) *Select {

	s.options = options

	return s
}

// Set function which will be executed if user made correct answer
func (s *Select) SetCallback(callback func(value string)) *Select {

	s.callback = callback

	return s
}

// Render question and option list
// Read text from command line
// Run callback function if answer is correct, ask again when answer is incorrect
func (s *Select) Run() *Select {
	inlineOptions := s.showInlineOptions()
	for {
		optionText := ""
		for i, option := range s.options {
			text := color(fmt.Sprintf("  %d ", i+1), YELLOW) + color(fmt.Sprintf("- %s", option), WHITE)
			if !inlineOptions {
				optionText += EOL
			}
			optionText += text
		}

		pl(s.question + optionText)
		number, _ := strconv.Atoi(input())
		if s.isCorrectAnswer(number) {
			if s.callback != nil {
				s.callback(s.options[number-1])
			}
			break
		} else {
			pl("BŁĄD: nieprawidłowy wybór", RED)
		}
	}

	return s
}

// Checking if question and all option labels could by print in one line or one per line
func (s *Select) showInlineOptions() bool {
	length := len(s.question)
	for _, option := range s.options {
		length += len(option)
	}

	return length < 80 && len(s.options) <= 2
}

// Checking if the answer is one of the defined options
func (s *Select) isCorrectAnswer(answer int) bool {
	for i := range s.options {

		if i == answer-1 {
			return true
		}
	}
	return false
}
