package main

import (
	"fmt"
	"github.com/mikkeloscar/sshconfig"
	"github.com/shiena/ansicolor"
	"io"
	"os"
	"runtime"
	"strings"
)

const (
	version = "1.02"
	// ścieżka do pliku konfiguracyjnego
	confFilePath = ".gogit_conf"
	// scieżka do logu
	gitLogPath = ".gogit_log"
	// ścieżka do listy wykluczonych plików
	gitExcludePath = ".git/info/exclude"
	// logowanie za pomocą hasła
	authTypePassword = "1"
	// logowanie za pomocą klucza prywatnego
	authTypeKey = "2"
)

var (
	// aktualne ścieżka zdalnego repozytorium
	remoteDir string
	// aktualne ścieżka lokalnego repozytorium
	currentDir string
	// referencja do konsoli
	console io.Writer
	// nazwa aktywnej branchy lokalnej
	currentBranchLocal string
	// nazwa aktywnej branchy zdalnej
	currentBranchRemote string
	// nazwa remota ustawiona w gicie
	remoteNameLocal string
	// referencja do obiektu klasy Git
	// zawiera wszystkie metody do komunikacji z gitem
	git Git
	// referencja do obiektu gogitConfig
	config gogitConfig
	// tutaj zapisujemy wszystko co się dzieje w serwisie
	globalLog []string
	// hash ostatniego commita lokalnej branchy
	currentLocalHash string
	// hash ostatniego commita zdalnej branchy
	currentRemoteHash string

	modules []string

	currentModule string

	hosts map[string]*sshconfig.SSHHost
)

func main() {

	currentDir, _ = os.Getwd()
	modules = checkModules()

	initConsole()

	InitializeGlobalMenu()
	InitializeSingleMenu()

	parseSystemConfigSSH()

	checkConfig()
	globalLoop()
}

// inicjalizacja biblioteki do kolorwania konsoli
func initConsole() {
	console = ansicolor.NewAnsiColorWriter(os.Stdout)
}

func showLocalLog() {
	fmt.Fprint(console, strings.Join(globalLog, EOL))
}

func isWindows() bool {
	return runtime.GOOS == "windows"
}
