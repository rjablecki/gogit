package main

import (
	"fmt"
	"github.com/mikkeloscar/sshconfig"
	"os/user"
	"strings"
)

func parseSystemConfigSSH() error {
	me, err := user.Current()

	if err != nil {
		pl("Error: " + err.Error())
		return err
	}

	hosts = make(map[string]*sshconfig.SSHHost)
	readHosts, err := sshconfig.ParseSSHConfig(fmt.Sprintf("%s"+SEP+".ssh"+SEP+"config", me.HomeDir))

	if err != nil {
		return err
	} else {
		for _, host := range readHosts {
			for _, name := range host.Host {
				hosts[name] = host
			}
		}
	}

	return nil
}

func showConfigSSH() bool {

	pl()
	err := parseSystemConfigSSH()

	if len(hosts) == 0 {
		if err != nil {
			pl(color(err.Error(), RED))
		} else {
			pl("Config file is empty or has invalid format", RED)
		}
	}

	for _, host := range hosts {
		pl()
		pl("Hosts: " + color(strings.Join(host.Host, ","), YELLOW))
		pl("HostName: " + color(host.HostName, YELLOW))
		pl("User: " + color(host.User, YELLOW))
		pl("Key: " + color(host.IdentityFile, YELLOW))
	}
	return true
}
