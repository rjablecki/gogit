package main

import (
	"fmt"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"unicode/utf8"
)

const (
	// LOCAL Git instance
	LOCAL = "LOCAL"
	// REMOTE Git instance
	REMOTE = "REMOTE"
	// All instance
	ALL = "ALL"
)

// Main class object contains all git methods
type Git struct{}

type branchList map[string]map[string]string

type gitStatusResponse struct {
	M     []string
	Mu    []string
	N     []string
	Nu    []string
	D     []string
	Du    []string
	A     []string
	Clear bool
	Ready bool
}

func (g *Git) getRemotePath() string {
	RemoteRes, _ := g.execute(LOCAL, "git remote -v")

	path := EMPTY_STRING
	lines := normalizeOutput(RemoteRes)

	for _, line := range lines {
		reg := regexp.MustCompile(`(/var.*)\s`)
		m := reg.FindAllStringSubmatch(line, -1)
		path = strings.Replace(m[0][0], ".git", EMPTY_STRING, -1)

		break
	}

	return path
}

func (g *Git) execute(source string, cmd string) (string, error) {
	if source == LOCAL {
		if isWindows() {
			cmd = "cmd /C " + cmd
		}
		r := execa(cmd, currentDir+SEP+currentModule)
		return r, nil
	}
	return runCmd("cd " + remoteDir + " && " + cmd)
}

func (g *Git) commit() {
	// sprawdzenie czy jest co commitowac
	// jesli nie to wywalamy blad
	statusRes, _ := g.execute(REMOTE, "git status --porcelain")
	status, _ := parseStatusGIT(statusRes)

	if status.Clear == true {
		pl("BŁĄD: nie ma co commitować", RED)
		return
	}
	pl("Podaj opis:" + EOL)
	message := strings.TrimSpace(input())
	if len(message) == 0 {
		pl("BŁĄD: musisz podać opis commita", RED)
		return
	}
	if len(message) < 5 {
		pl("BŁĄD: opis za krótki. Minimum 5 znaków", RED)
		return
	}

	message = strings.Replace(message, "'", "'\"'\"'", -1)
	commitResponse, _ := g.execute(REMOTE, fmt.Sprintf("git add . && git commit -m  '%s'", message))
	pl(commitResponse)

	g.execute(LOCAL, "git fetch -p "+remoteNameLocal)

	res, _ := g.execute(LOCAL, "git branch -va")
	localBranches := git.parseBranchesGIT(LOCAL, res)
	g.execute(LOCAL, "git add . && git stash")
	branchName := currentBranchRemote
	if _, ok := localBranches[branchName]; ok {
		if currentBranchLocal != branchName {
			g.execute(LOCAL, "git checkout "+branchName)
		}
		g.execute(LOCAL, "git merge "+remoteNameLocal+SEP+branchName)
	} else {
		g.execute(LOCAL, fmt.Sprintf("git checkout -b %s %s/%s", branchName, remoteNameLocal, branchName))
	}
	// aktualizujemy w headerze nazwy aktualnych branchy i odwiezamy
	currentBranchRemote = branchName
	currentBranchLocal = branchName
	clearConsole()
	printHeader()
	pl("SUKCES: commit dodany", GREEN)
}

func (g *Git) validateBranchName(name string) bool {
	name = strings.TrimSpace(name)
	if len(name) == 0 {
		pl("BŁĄD: nazwa nie może być pusta", RED)
		return false
	}
	matchPattern := regexp.MustCompile(`^[a-zA-Z0-9_]+$`).MatchString(name)
	if matchPattern == false {
		pl("BŁĄD: nazwa zawiera nieprawidłowe znaki ^[a-zA-Z0-9_]+$", RED)
		return false
	}

	if len(name) > 80 {
		pl("BŁĄD: nazwa jest za długa. Max 80 znaków", RED)
		return false
	}

	return true
}

func (g *Git) createNew() {
	statusRes, _ := g.execute(REMOTE, "git status --porcelain")
	status, _ := parseStatusGIT(statusRes)
	if status.Clear == false {
		pl("BŁĄD: masz nieskomitowane zmiany na serwerze", RED)
		return
	}
	pl(EOL+" Podaj nazwę nowej branchy:"+EOL, YELLOW)
	newBranchName := input()

	if g.validateBranchName(newBranchName) == false {
		return
	}

	res, _ := g.execute(REMOTE, "git branch -va")
	remoteBranches := g.parseBranchesGIT(REMOTE, res)
	currentBranchRemote, currentRemoteHash = getCurrentBranch(remoteBranches)
	branchNames := make([]string, 0, len(remoteBranches))
	for k := range remoteBranches {
		branchNames = append(branchNames, k)
	}
	pl(EOL+" Na podstawie której:"+EOL, YELLOW)

	sort.Strings(branchNames)
	for key, branch := range branchNames {
		branch = strings.Replace(branch, "remotes/", EMPTY_STRING, -1)
		if len(branch) > 45 {
			branch = branch[:45] + "..."
		}
		pl(color(fmt.Sprintf("%d", key+1), YELLOW) + color(" - ", WHITE) + color(branch, CYAN))
	}
	number, _ := strconv.Atoi(input())
	if number < 1 || number > len(branchNames) {
		pl("BŁĄD: nie ma takiej branchy", RED)
		return
	}
	oldBranchName := branchNames[number-1]
	oldBranchName = strings.Replace(oldBranchName, "remotes/", EMPTY_STRING, -1)

	g.execute(REMOTE, "git branch -D "+newBranchName)
	g.execute(REMOTE, fmt.Sprintf("git checkout -b %s %s", newBranchName, oldBranchName))

	// sprawdzenie czy istniej lokalna o takiej nazwie
	// jesli jest to mergujemy
	// jesli nie to tworzymy nowa na podstawie zdalnej
	//@todo zrobić fetcha jeśli to potrzebne
	res, _ = g.execute(LOCAL, "git fetch -p "+remoteNameLocal)
	res, _ = g.execute(LOCAL, "git branch -va")
	localBranches := g.parseBranchesGIT(LOCAL, res)

	g.execute(LOCAL, "git add . && git stash")
	if _, ok := localBranches[newBranchName]; ok {
		g.execute(LOCAL, "git branch -D "+newBranchName)
	}

	g.execute(LOCAL, fmt.Sprintf("git checkout -b %s %s/%s", newBranchName, remoteNameLocal, newBranchName))

	// aktualizujemy w headerze nazwy aktualnych branchy i odwiezamy
	currentBranchRemote = newBranchName
	currentBranchLocal = newBranchName
	clearConsole()
	printHeader()
	pl(color("SUKCES: zmieniono branche na ", GREEN) + color(newBranchName, YELLOW))
}

func (g *Git) branch(source string) {
	res, _ := g.execute(source, "git branch -va")
	branchList := g.parseBranchesGIT(source, res)
	if source == REMOTE {
		currentBranchRemote, currentRemoteHash = getCurrentBranch(branchList)
	}
	g.showBranchesGIT(branchList, ALL)
}

func (g *Git) status(source string) {
	res, _ := g.execute(source, "git status --porcelain")
	showStatusGIT(res)
}

func (g *Git) isClear() bool {
	res, _ := g.execute(REMOTE, "git status --porcelain")
	status, _ := parseStatusGIT(res)
	return status.Clear
}

func (g *Git) checkout() {
	statusRes, _ := g.execute(REMOTE, "git status --porcelain")
	status, _ := parseStatusGIT(statusRes)
	if status.Clear == false {
		pl("BŁĄD: masz nieskomitowane zmiany na serwerze", RED)
		return
	}
	res, _ := g.execute(REMOTE, "git branch -v")
	remoteBranches := g.parseBranchesGIT(REMOTE, res)
	currentBranchRemote, currentRemoteHash = getCurrentBranch(remoteBranches)
	branchNames := make([]string, 0, len(remoteBranches))

	for k := range remoteBranches {
		branchNames = append(branchNames, k)
	}
	sort.Strings(branchNames)
	pl(EOL+" Którą branchę chcesz uruchomić:"+EOL, YELLOW)
	for key, branch := range branchNames {
		if len(branch) > 45 {
			branch = branch[:45] + "..."
		}
		pl(color(fmt.Sprintf("%d", key+1), YELLOW) + color(" - ", WHITE) + color(branch, CYAN))
	}

	number, _ := strconv.Atoi(input())

	if number < 1 || number > len(branchNames) {
		pl("BŁĄD: nie ma takiej branchy", RED)
		return
	}

	branchName := branchNames[number-1]
	if branchName != currentBranchRemote {
		g.execute(REMOTE, "git checkout "+branchName)
	}

	g.execute(LOCAL, "git fetch -p "+remoteNameLocal)
	g.RemoveLocalBranchIfExists(LOCAL, "gogit_tmp")
	g.execute(LOCAL, "git add . && git stash")
	g.execute(LOCAL, "git checkout -b gogit_tmp")
	g.RemoveLocalBranchIfExists(LOCAL, branchName)
	g.execute(LOCAL, fmt.Sprintf("git checkout -b %s %s/%s", branchName, remoteNameLocal, branchName))
	g.RemoveLocalBranchIfExists(LOCAL, "gogit_tmp")

	// aktualizujemy w headerze nazwy aktualnych branchy i odswiezamy
	currentBranchRemote = branchName
	currentBranchLocal = branchName
	clearConsole()
	printHeader()
	pl(color("SUKCES: zmieniono branche na ", GREEN) + color(branchName, YELLOW))
}

func (g *Git) CheckoutLocalRepoLikeServer() {
	remoteNameLocal = git.getRemoteName(LOCAL)
	remoteDir = git.getRemotePath()

	git.execute(LOCAL, "git fetch "+remoteNameLocal)
	res, _ := git.execute(REMOTE, "git branch -v")
	remoteBranches := git.parseBranchesGIT(REMOTE, res)

	for branchName, branch := range remoteBranches {
		if branch["is_active"] == "1" {
			git.execute(LOCAL, fmt.Sprintf("git checkout -b %s %s/%s", branchName, remoteNameLocal, branchName))

			currentBranchRemote = branchName
			currentBranchLocal = branchName
			break
		}
	}
}

func (g *Git) RemoveLocalBranchIfExists(source, branchName string) {
	res, _ := g.execute(source, "git branch -va")
	localBranches := g.parseBranchesGIT(source, res)
	if source == LOCAL {
		currentBranchLocal, currentLocalHash = getCurrentBranch(localBranches)
	}

	if _, ok := localBranches[branchName]; ok {
		g.execute(source, "git branch -D "+branchName)
	}
}

func (g *Git) getRemoteName(source string) string {
	res, err := g.execute(source, "git remote")
	if err != nil {
		dump(err)
		return EMPTY_STRING
	}
	var remotes []string

	lines := normalizeOutput(res)
	for _, remote := range lines {
		remotes = append(remotes, remote)
	}
	if len(remotes) == 0 {
		return EMPTY_STRING
	}
	return remotes[0]
}

func (g *Git) showLog(source string) {
	pl(source+" LOG:"+EOL, PINK)

	log, _ := g.execute(source, "git log -n 60 --no-merges --pretty=format:%h_%ct_%cn_%s")
	lines := normalizeOutput(log)
	for _, line := range lines {
		pattern := regexp.MustCompile(`^(.*)_(.*)_(.*)_(.*)$`)
		matches := pattern.FindAllStringSubmatch(line, -1)

		hash := matches[0][1]
		timestamp, err := strconv.Atoi(matches[0][2])

		if err != nil {
			continue
		}
		date := date("d.m H:i", int64(timestamp))
		user := g.getShortName(matches[0][3])
		subject := matches[0][4]
		if len(subject) > 70 {
			subject = subject[:70]
		}
		pl(color(date, CYAN) + " " + color(hash, BLUE) + " " + color(user, GREEN) + " " + color(subject, YELLOW))
	}
}

func (g *Git) fetch() {
	remoteName := git.getRemoteName(REMOTE)
	if remoteName != "gitlab" {
		pl("BŁĄD: nie ma gitlaba", RED)
		return
	}
	var branches branchList
	var res string
	pl("PRZED", PINK)
	responseBranchPrev, _ := g.execute(REMOTE, "git branch -va")
	branches = git.parseBranchesGIT(REMOTE, responseBranchPrev)
	git.showBranchesGIT(branches, REMOTE)

	res, _ = g.execute(REMOTE, "git fetch -p -v gitlab")

	pl(EOL+" PO", PINK)
	responseBranchAfter, _ := g.execute(REMOTE, "git branch -va")
	branches = git.parseBranchesGIT(REMOTE, responseBranchAfter)
	git.showBranchesGIT(branches, REMOTE)

	if len(res) == 0 {
		//pl("INFO: nie ma żadnych zmian. Wszystko jest aktualne", CYAN)
	} else {
		pl(res)
	}
}

func (g *Git) merge() {
	statusRes, _ := g.execute(REMOTE, "git status --porcelain")
	status, _ := parseStatusGIT(statusRes)

	if status.Clear == false {
		pl("BŁĄD: masz nieskomitowane zmiany na serwerze", RED)
		return
	}

	res, _ := g.execute(REMOTE, "git branch -va")
	remoteBranches := g.parseBranchesGIT(REMOTE, res)
	currentBranchRemote, currentRemoteHash = getCurrentBranch(remoteBranches)
	branchNames := make([]string, 0, len(remoteBranches))

	for k := range remoteBranches {
		k = strings.Replace(k, "remotes/", EMPTY_STRING, -1)
		branchNames = append(branchNames, k)
	}
	pl(color(EOL+" Wybierz branchę, którą chcesz wmergować do branchy ", WHITE) + color(currentBranchRemote, YELLOW) + color(":"+EOL, WHITE))
	sort.Strings(branchNames)
	for key, branch := range branchNames {
		if len(branch) > 45 {
			branch = branch[:45] + "..."
		}

		pl(color(fmt.Sprintf("%d", key+1), YELLOW) + color(" - ", WHITE) + color(branch, CYAN))
	}

	number, _ := strconv.Atoi(input())

	if number < 1 || number > len(branchNames) {
		pl("BŁĄD: nie ma takiej branchy", RED)
		return
	}
	branchName := branchNames[number-1]
	if branchName == currentBranchRemote {
		pl("BŁĄD: nie można mergować aktualnej branchy", RED)
		return
	}

	mergeResponse, _ := g.execute(REMOTE, "git merge "+branchName)

	if strings.Contains(mergeResponse, "CONFLICT") {
		clearConsole()
		printHeader()
		pl(mergeResponse, WHITE)
		pl()
		pl(color("KONFLIKT - MERGE ANULOWANY", RED))
		g.execute(REMOTE, "git merge --abort ")
		return
	}

	if strings.Contains(mergeResponse, "Already up-to-date") {
		clearConsole()
		printHeader()
		pl("INFO: wszystko jest aktualnie. Nie ma co mergować", CYAN)
		return
	}

	g.checkoutLocal(branchName)

	// aktualizujemy w headerze nazwy aktualnych branchy i odwiezamy
	currentBranchRemote = branchName
	currentBranchLocal = branchName

	clearConsole()
	printHeader()
	pl(color("SUKCES: pomyślnie wmergowano branchę ", GREEN) + color(branchName, YELLOW) + color(" do branchy ", GREEN) + color(currentBranchRemote, YELLOW))
	pl()
	pl(mergeResponse, WHITE)
}

func (g *Git) checkoutLocal(branchName string) {
	g.execute(LOCAL, "git fetch -p "+remoteNameLocal)
	g.RemoveLocalBranchIfExists(LOCAL, "gogit_tmp")
	g.execute(LOCAL, "git add . && git stash")
	g.execute(LOCAL, "git checkout -b gogit_tmp")
	g.RemoveLocalBranchIfExists(LOCAL, branchName)
	g.execute(LOCAL, fmt.Sprintf("git checkout -b %s %s/%s", branchName, remoteNameLocal, branchName))
	g.RemoveLocalBranchIfExists(LOCAL, "gogit_tmp")
}

func (g *Git) push(customName bool) {
	cmd := currentBranchRemote
	msg := EMPTY_STRING
	if customName {
		pl(EOL+" Podaj nazwę branchy pod jaką chcesz wypchnąć branche '"+currentBranchRemote+"'?"+EOL, YELLOW)
		newBranchName := input()

		if g.validateBranchName(newBranchName) == false {
			return
		}
		cmd += ":" + newBranchName
		msg = color(" jako ", GREEN) + color(newBranchName, YELLOW)
	}

	res, _ := g.execute(REMOTE, "git push gitlab "+cmd)
	pl(res)
	pl(color("SUKCES: Wypchnięto branchę ", GREEN) + color(currentBranchRemote, YELLOW) + msg)
	pl()
}

func (g *Git) showBranchesGIT(branches branchList, show string) {
	l := map[string]int{
		"name":    0,
		"author":  0,
		"subject": 0,
	}

	for _, b := range branches {

		nameLen := len(b["name"])

		if nameLen > 45 {
			nameLen = 45
		}

		if nameLen > l["name"] {
			l["name"] = nameLen + 1
		}
		if len(b["author"]) > l["author"] {
			l["author"] = len(b["author"]) + 1
		}
		if len(b["subject"]) > l["subject"] {
			l["subject"] = len(b["subject"]) + 1
		}
	}

	linesRemoteS := map[int]string{}
	linesLocalS := map[int]string{}
	anyLocal := false
	anyRemote := false

	cnt := 0
	for _, b := range branches {
		active := "  "
		cnt++

		if b["is_active"] == "1" {
			active = color("» ", PINK)
		}
		TimestampInt, _ := strconv.Atoi(b["timestamp"])

		branchName := b["name"]

		if len(branchName) > 45 {
			branchName = branchName[:45]
		}

		line := active
		line += color(branchName, CYAN) + strings.Repeat(" ", l["name"]-len(branchName))
		line += color(date("d.m H:i", int64(TimestampInt)), BLUE) + " "
		line += color(b["author"], GREEN) + strings.Repeat(" ", l["author"]-utf8.RuneCountInString(b["author"]))
		line += color(b["subject"], YELLOW) + strings.Repeat(" ", l["subject"]-utf8.RuneCountInString(b["subject"]))

		if b["is_remote"] == "1" {
			anyRemote = true
			linesRemoteS[TimestampInt+cnt] = line
		} else {
			anyLocal = true
			linesLocalS[TimestampInt+cnt] = line
		}
	}

	cnt++

	if anyLocal && (show == ALL || show == LOCAL) {
		pl("LOKALNE:"+EOL, YELLOW)
		keysLocal := make([]int, 0, len(linesLocalS))
		for k := range linesLocalS {
			keysLocal = append(keysLocal, k)
		}
		sort.Sort(sort.Reverse(sort.IntSlice(keysLocal)))
		for _, i := range keysLocal {
			pl(linesLocalS[i])
		}
	}
	if anyRemote && (show == ALL || show == REMOTE) {
		pl(EOL+" ZDALNE:"+EOL, YELLOW)
		keysRemote := make([]int, 0, len(linesRemoteS))
		for k := range linesRemoteS {
			keysRemote = append(keysRemote, k)
		}
		sort.Sort(sort.Reverse(sort.IntSlice(keysRemote)))
		for _, i := range keysRemote {
			pl(linesRemoteS[i])
		}
	}
}

func (g *Git) parseBranchesGIT(source string, res string) branchList {
	lines := strings.Split(res, EOL)
	branches := make(branchList)

	for _, oneLine := range lines {
		trimed := strings.TrimSpace(oneLine)
		if len(trimed) == 0 {
			continue
		}
		aaa := regexp.MustCompile(`\s*(\*)?\s*(\S*)\s*(\S*)\s*(.*)$`)
		result := aaa.FindAllStringSubmatch(oneLine, -1)
		branch := make(map[string]string)

		if strings.Index(result[0][2], "remotes") == 0 {
			branch["is_remote"] = "1"
			branch["name"] = result[0][2][8:]
		} else {
			branch["is_remote"] = "0"
			branch["name"] = result[0][2]
		}
		if result[0][1] == "*" {
			branch["is_active"] = "1"
		} else {
			branch["is_active"] = "0"
		}

		branch["hash"] = result[0][3]
		branch["commit"] = result[0][4]
		branches[result[0][2]] = branch
	}

	var cmds []string
	for name, branch := range branches {
		cmds = append(cmds, "git log "+branch["hash"]+" -n 1 --pretty=tformat:%cn###%cr###%ct###%s###"+name)
	}
	commitsData, _ := g.execute(source, strings.Join(cmds, " && "))
	commitLines := strings.Split(commitsData, EOL)

	for _, lineNotTrimed := range commitLines {
		line := strings.TrimSpace(lineNotTrimed)
		if len(line) == 0 {
			continue
		}
		reg := regexp.MustCompile(`^(.*)###(.*)###(.*)###(.*)###(.*)$`)
		m := reg.FindAllStringSubmatch(line, -1)

		branchName := m[0][5]

		subject := m[0][4]
		if len(subject) > 48 {
			subject = subject[:48]
		}
		branches[branchName]["subject"] = subject
		branches[branchName]["author"] = g.getShortName(m[0][1])
		branches[branchName]["time"] = m[0][2]
		branches[branchName]["timestamp"] = m[0][3]
	}
	return branches
}

func (g *Git) getShortName(name string) string {
	authors := strings.Split(name, " ")
	return strings.ToLower(authors[0])
}

func getCurrentBranch(branches branchList) (string, string) {
	for _, b := range branches {
		if b["is_active"] == "1" {
			hash := b["hash"]
			if len(hash) > 3 {
				hash = hash[:3]
			}
			return b["name"], hash
		}
	}
	return EMPTY_STRING, EMPTY_STRING
}
