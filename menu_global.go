package main

type menuItem struct {
	name        string
	label       string
	description string
	action      func() bool
}

var globalMenu []menuItem

func InitializeGlobalMenu() {

	globalMenu = append(globalMenu, menuItem{"1", "sprawdź statusy", "sprawdzenie czy są jakieś zmiany w wersjach na serwerze", checkStatusAll})
	globalMenu = append(globalMenu, menuItem{"2", "wyrównaj LOCAL <= REMOTE", "wyrównanie wersji lokalnych z wersjami na serwerze (jeśli nie ma żadnych zmian na serwerze)", synchroniseClear})
	globalMenu = append(globalMenu, menuItem{"3", "wybierz repozytorium", "przejście do akcji na wybranym repozytorium", selectRepo})
	globalMenu = append(globalMenu, menuItem{"4", "pokaż konfigurację", "wyświetlenia obecnej konfiguracji połączenia SSH", func() bool { return showConfiguration(config) }})
	globalMenu = append(globalMenu, menuItem{"5", "uruchom konfigurację", "uruchomienie procesu nadpisania konfiguracji połączenia SSH", runConfiguration})
	globalMenu = append(globalMenu, menuItem{"6", "pokaż config ssh", "listuje ustawienia z pliku ~/.ssh/config", showConfigSSH})
	globalMenu = append(globalMenu, menuItem{"7", "dodaj repo", "dodanie nowe repo, inicjalizacja gita i połączenie z serwerem i checkoutuje lokala", addRepo})

}
