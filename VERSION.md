2.09

##2.09 (2020-03-17)##
* fix - lista branchy - poprawienie szerokości kolumn, gdy użyto znaków utf8
* fix - linux - naprawienie ładowania systemowych configów SSH
* fix - linux - poprawienie czyszczenia konsoli

##2.08 (2018-12-14)##
* fix - stashowanie lokalnych zmian, gdy wyrównujemy wszystkie repozytoria 
* fix - poprawienie wyboru typu audentykacji

##2.07 (2018-11-24)##
* dodanie nowej struktury Select - obsługującej renderowanie i obsługę wyboru opcji przez użytkownika

##2.06 (2018-11-22)##
* git log pomija commity, które są tylko mergowaniem branchy

##2.05 (2018-11-15)##
* dodane repo lokalne zostaje automatycznie zsynchronizowane z remotem
* refaktoryzacja kodu

##2.04 (2018-10-23)##
* możliwość automatycznego dodania repo lokalnego na podstawie repozytorium zdalnego

##2.03 (2018-10-18)##
* możliwość ustawienia konfiguracji na podstawie wybranego konfigu ssh
* rozszerzenie konfiguracji o opcję włączania i wyłączania logowania do pliku
* logowanie komend do pliku domyślnie wyłączone

##2.02 (2018-08-08)##
* listowanie hostów zdefiniowanych w konfigu ssh

##2.01 (2018-08-07)##
* escepowanie apostrofu przy commitowaniu
* walidacja commit message

##2.00 (2018-07-12)##
* uruchomienie aplikacji dla linuxa

##1.02 (2015-12-03)##
* poprawienie błędu wyświetlającego komunikat o zmieni gałęzi, gdy występował błąd połączenia SSH 
* dodanie pliku ze zmianami w programie 
* sprawdzanie czy istnieje nowsza wersja oprogramowania

##1.01 (2015-11-07)##
* obok nazwy aktualnej gałęzi widoczny jest hash ostatniego commita 
* wyswietlanie informacji o błędach w połączeniu z serwerem

##1.00 (2015-10-29)##
* okresowe sprawdzanie zgodności branchy lokalnej i zdalnej
