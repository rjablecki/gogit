package main

import (
	"sort"
	"strconv"
)

func selectServer() {

	servers := make([]string, 0, len(hosts))
	for name := range hosts {
		servers = append(servers, name)
	}

	sort.Strings(servers)

	serverSelect := NewSelect("Którego serwera chcesz używać:")
	serverSelect.SetOptions(servers)
	serverSelect.SetCallback(func(serverName string) {
		serverConfig := hosts[serverName]

		tmpConf := getDefaultConfig()
		tmpConf.Host = serverConfig.HostName
		tmpConf.Port = strconv.Itoa(serverConfig.Port)
		tmpConf.User = serverConfig.User
		tmpConf.AuthType = authTypeKey
		tmpConf.PrivateKey = serverConfig.IdentityFile
		tmpConf.ServerHost = serverName

		showConfiguration(tmpConf)
		showSaveConfigConfirmation(tmpConf)
	})

	serverSelect.Run()
}
