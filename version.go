package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"os/user"
	"strings"
)

const (
	versionFilePath = "https://bitbucket.org/api/1.0/repositories/anthracis/gogit/raw/master/VERSION.md"
	binSourcePath   = "https://bitbucket.org/api/1.0/repositories/anthracis/gogit/raw/master/bin/gogit.exe"
)

func checkNewestVersion() {

	newestVersion, err := getNewestVersion()

	if err != nil {
		return
	}

	if newestVersion == version {
		return
	}

	clearConsole()

	text := fmt.Sprintf("Jest nowsza wersja gogita (%s). Twoja wersja to (%s). Czy chcesz pobrać aktualizację ?", newestVersion, version)

	pl(text, YELLOW)
	pl("1 - tak", GREEN)
	pl("2 - nie", RED)
	pl("", WHITE)

	for {
		cmd := input()
		switch cmd {
		case "1":
			download(newestVersion)
		case "2":
			break

		default:
			pl("BŁĄD: nie ma takiej opcji", RED)
		}
	}
}

func download(v string) {

	me, _ := user.Current()
	dir := me.HomeDir + "/Desktop/"

	source := binSourcePath
	destination := dir + "gogit_" + v + ".exe"

	if _, err := os.Stat(destination); os.IsNotExist(err) {
		downloadFromUrl(source, destination)
		return
	}
}

func getNewestVersion() (string, error) {
	resp, err := http.Get(versionFilePath)

	if err != nil {
		fmt.Println("Błąd podczas pobierania informacji o najnowszej wersji")
		return "", err
	}
	defer resp.Body.Close()
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	body := string(bodyBytes)
	lines := strings.Split(body, EOL)

	return strings.TrimSpace(lines[0]), nil
}

func downloadFromUrl(url string, filename string) {

	output, err := os.Create(filename)
	if err != nil {
		fmt.Println("Error while creating", filename, "-", err)
		return
	}
	defer output.Close()

	response, err := http.Get(url)
	if err != nil {
		fmt.Println("Error while downloading", url, "-", err)
		return
	}
	defer response.Body.Close()

	n, err := io.Copy(output, response.Body)
	if err != nil {
		fmt.Println("Error while downloading", url, "-", err)
		return
	}
	fmt.Println(n, "bytes downloaded.")
}
