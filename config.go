package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

type gogitConfig struct {
	Host       string
	Port       string
	User       string
	Password   string
	AuthType   string
	PrivateKey string
	ServerHost string
	Logging    bool
}

// wyświetlenie całej konfiguracji
func showConfiguration(c gogitConfig) bool {
	printGlobalHeader()
	pl()
	pl()
	pl(color("host: ", WHITE) + color(c.Host, YELLOW))
	pl(color("port: ", WHITE) + color(c.Port, YELLOW))
	pl(color("user: ", WHITE) + color(c.User, YELLOW))

	var authType string
	var pass string

	if c.AuthType == authTypePassword {
		authType = "hasło"
		pass = color("hasło: ", WHITE) + color(c.Password, YELLOW)
	} else if c.AuthType == authTypeKey {
		authType = "klucz prywatny"
		pass = color("nazwa klucza: ", WHITE) + color(c.PrivateKey, YELLOW)
	}
	pl(color("typ logowania: ", WHITE) + color(authType, YELLOW))
	pl(pass)

	logging := "tak"
	if !c.Logging {
		logging = "nie"
	}

	pl(color("plik z logami: ", WHITE) + color(logging, YELLOW))

	return true
}

// uruchomienie konfiguracji
func runConfiguration() bool {
	clearConsole()
	pl(EOL+" KONFIGURACJA:", YELLOW)
	fmt.Fprintln(console, strings.Repeat("_", 100))

	pl(EOL + "Nie znaleziono pliku konfiguracyjnego." + EOL)

	question := "W pliku ~/.ssh/config masz zdefiniowane serwery [" + fmt.Sprintf("%d", len(hosts)) + "]. Chcesz wybrać konfiguracje serwera, czy wpisać wszystko ręcznie?"
	options := []string{"użyj serwera", "uzupełnij wszystko ręcznie"}

	configurationSelect := NewSelect(question)
	configurationSelect.SetOptions(options)
	configurationSelect.SetCallback(func(selected string) {
		if selected == options[0] {
			selectServer()
		} else {
			manualConfiguration()
		}
	})

	configurationSelect.Run()

	return true
}

func manualConfiguration() {
	prevInfo := EMPTY_STRING
	if config.Host != EMPTY_STRING {
		showConfiguration(config)
	} else {
		prevInfo = "Jeśli uruchamiasz aplikacje po raz pierwszy pamiętaj aby: " + EOL + " - repozytoria były już zainicjalizowane " + EOL + " - miały dodanego remota " + EOL + " - miały już wycheckoutowane branche na podstawie serwerowej." + EOL
	}

	pl(EOL + prevInfo + " Uzupełnij dane do połączenia z serwerem przez ssh. Ustawienia powinny wyglądać jak w pliku. ~/.ssh/config" + EOL)
	pl("Podaj host lub ip [np. 192.168.7.91]:")
	host := input()
	pl("Podaj port [np. 22]: ")
	port := input()
	pl("Podaj użytkownika [np. jan.kowalski]:")
	user := input()

	var authType, password, privateKey string

	options := []string{"hasło", "klucz prywatny"}

	authTypeSelect := NewSelect("Wybierz typ logowania:")
	authTypeSelect.SetOptions(options)
	authTypeSelect.SetCallback(func(value string) {
		if value == options[0] {
			pl("Podaj hasło:")
			password = input()
			authType = authTypePassword
		} else {
			authType = authTypeKey
			pl("Którego klucza chcesz użyć do logowania ~/.ssh/ wpisz nazwę pliku:")
			listDirectorySSH()
			pl(EOL)
			//@todo przerobić to na wybieranie z listy
			privateKey = input()
		}
	})
	authTypeSelect.Run()

	tmpConf := getDefaultConfig()
	tmpConf.Host = host
	tmpConf.Port = port
	tmpConf.User = user
	tmpConf.Password = password
	tmpConf.AuthType = authType
	tmpConf.PrivateKey = privateKey

	showConfiguration(tmpConf)

	showSaveConfigConfirmation(tmpConf)
}

func showSaveConfigConfirmation(conf gogitConfig) {

	saveSelect := NewSelect("Czy chcesz zapisać ustawienia ?")
	saveSelect.SetCallback(func(saving string) {
		if saving == YES {
			emptyConfig, err := json.MarshalIndent(conf, EMPTY_STRING, TAB)
			if err != nil {
				pl(err.Error())
			}
			errorWriteFile := ioutil.WriteFile(confFilePath, []byte(emptyConfig), 0777)
			if errorWriteFile != nil {
				dump("Błąd podczas zapisywania pliku konfiguracyjnego")
			}
			config = conf
		}
	})
	saveSelect.Run()
	clearConsole()
	printHeader()
}

// sprawdzenie i wczytanie pliku konfiguracyjnego
// jeśli plik nie istnieje to jest tworzony
// jeśli plik nie zawiera prawidłowych opcji wychodzimy z aplikacji
func checkConfig() {
	// wczytnie pliku konfiguracyjnego
	configFile, err := ioutil.ReadFile(confFilePath)
	if err != nil {
		runConfiguration()
		checkConfig()
		return
	}
	// wczytanie danych do zmiennej
	jsonError := json.Unmarshal(configFile, &config)
	if jsonError != nil {
		fmt.Print("Nie udalo sie odczytaj JSONA ", err)
		os.Exit(1)
	}
}

func getDefaultConfig() gogitConfig {
	return gogitConfig{
		Logging: false,
	}
}
