package main

var singleMenu []menuItem

func InitializeSingleMenu() {

	singleMenu = append(singleMenu, menuItem{"1", "status (L)", "", func() bool {
		git.status(LOCAL)

		return true
	}})
	singleMenu = append(singleMenu, menuItem{"2", "status (R)", "", func() bool {
		git.status(REMOTE)

		return true
	}})
	singleMenu = append(singleMenu, menuItem{"3", "branche (L)", "", func() bool {
		git.branch(LOCAL)

		return true
	}})
	singleMenu = append(singleMenu, menuItem{"4", "branche (R)", "", func() bool {
		git.branch(REMOTE)

		return true
	}})
	singleMenu = append(singleMenu, menuItem{"5", "commit", "", func() bool {
		git.commit()

		return true
	}})
	singleMenu = append(singleMenu, menuItem{"6", "checkout / refresh", "", func() bool {
		git.checkout()

		return true
	}})
	singleMenu = append(singleMenu, menuItem{"7", "fetch gitlab", "", func() bool {
		git.fetch()

		return true
	}})
	singleMenu = append(singleMenu, menuItem{"8", "mergowanie do aktualnej branchy", "", func() bool {
		git.merge()

		return true
	}})
	singleMenu = append(singleMenu, menuItem{"9", "nowa brancha", "", func() bool {
		git.createNew()

		return true
	}})
	singleMenu = append(singleMenu, menuItem{"10", "local log", "", func() bool {
		git.showLog(LOCAL)

		return true
	}})
	singleMenu = append(singleMenu, menuItem{"11", "remote log", "", func() bool {
		git.showLog(REMOTE)

		return true
	}})
	singleMenu = append(singleMenu, menuItem{"12", "pokaż co się dzieje", "", func() bool {
		showLocalLog()

		return true
	}})
	singleMenu = append(singleMenu, menuItem{"13", "push aktualnej branchy do gitlaba", "", func() bool {
		git.push(false)

		return true
	}})
	singleMenu = append(singleMenu, menuItem{"13a", "push aktualnej branchy do gitlaba z inną nazwą", "", func() bool {
		git.push(true)

		return true
	}})
	singleMenu = append(singleMenu, menuItem{"0", "powrót do głównego menu", "", func() bool {
		unsetModule()

		return true
	}})

}
