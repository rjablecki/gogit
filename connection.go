package main

import (
	"bytes"
	"fmt"
	"golang.org/x/crypto/ssh"
	"io/ioutil"
	"net"
	"os/exec"
	"os/user"
	"strings"
	"time"
)

// run local command
func execa(cmd string, dir string) string {

	var out bytes.Buffer
	var stderr bytes.Buffer
	var command *exec.Cmd

	if isWindows() {
		cmdArgs := strings.Split(cmd, " ")
		command = exec.Command("cmd", cmdArgs...)
	} else {
		cmd = strings.Replace(cmd, "&&", ";", -1)
		command = exec.Command("/bin/sh", "-c", cmd)
	}

	timeStart := time.Now()
	command.Dir = dir
	command.Stdout = &out
	command.Stderr = &stderr

	err := command.Run()
	if err != nil {
		fmt.Println(fmt.Sprint(err) + ": " + stderr.String())
		return EMPTY_STRING
	}
	tmEnd := time.Now().Sub(timeStart).Nanoseconds()
	loggerAll(LOCAL, cmd, out.String(), tmEnd)

	return out.String()
}

// run remote command
func runCmd(cmd string) (string, error) {
	timeStart := time.Now()
	session, _, err := makeConnection()

	if err != nil {
		return EMPTY_STRING, err
	}
	//defer client.Close()
	defer session.Close()

	var b bytes.Buffer
	session.Stdout = &b
	err = session.Run(cmd)
	if err != nil {
		return EMPTY_STRING, err
	}
	timeEnd := time.Now().Sub(timeStart).Nanoseconds()
	loggerAll(REMOTE, cmd, b.String(), timeEnd)
	return b.String(), nil
}

func listDirectorySSH() {
	me, _ := user.Current()
	dir := me.HomeDir + "/.ssh"
	files, _ := ioutil.ReadDir(dir)
	for _, f := range files {
		pl("- " + f.Name())
	}
}

func PublicKeyFile(file string) ssh.AuthMethod {
	buffer, err := ioutil.ReadFile(file)
	if err != nil {
		exit("nie udało się odnaleźć klucza: " + file + " " + err.Error())
	}
	key, err := ssh.ParsePrivateKey(buffer)
	if err != nil {
		exit("coś nie tak z kluczem: " + file + " " + err.Error())
	}
	return ssh.PublicKeys(key)
}

func getPrivateKeyPath() string {
	me, _ := user.Current()

	prefix := EMPTY_STRING
	if !strings.Contains(config.PrivateKey, SEP) {
		prefix = me.HomeDir + "/.ssh/"
	}
	config.PrivateKey = strings.Replace(config.PrivateKey, "~", me.HomeDir, -1)

	return prefix + config.PrivateKey
}

var (
	clients map[string]*ssh.Client
)

func makeConnection() (*ssh.Session, *ssh.Client, error) {

	if clients == nil {
		clients = make(map[string]*ssh.Client)
	}
	address := config.Host + ":" + config.Port
	if _, ok := clients[address]; ok == false {
		configSSH := &ssh.ClientConfig{
			User: config.User,
			HostKeyCallback: func(hostname string, remote net.Addr, key ssh.PublicKey) error {
				return nil
			},
		}
		if config.AuthType == authTypePassword {
			configSSH.Auth = []ssh.AuthMethod{ssh.Password(config.Password)}
		} else if config.AuthType == authTypeKey {
			publicKey := PublicKeyFile(getPrivateKeyPath())
			configSSH.Auth = []ssh.AuthMethod{publicKey}
		} else {
			exit("nieprawidłowy typ logowania: " + config.AuthType)
		}

		ca, err := ssh.Dial("tcp", address, configSSH)
		if err != nil {
			date := date("d.m H:i:s", time.Now().Unix())
			pl(color(date+" nie udało się połączyć z hostem: "+address+" "+err.Error(), RED))
			return nil, nil, err
		}
		clients[address] = ca
	}

	client := clients[address]
	session, err := client.NewSession()
	if err != nil {
		pl(err.Error())
		exit("coś nie tak z sesją")
	}
	return session, client, err
}
