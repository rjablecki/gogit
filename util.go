package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

const (
	// RED color
	RED = "31"
	// GREEN color
	GREEN = "32"
	// YELLOW color
	YELLOW = "33"
	// BLUE color
	BLUE = "34"
	// PINK color
	PINK = "35"
	// CYAN color
	CYAN = "36"
	// WHITE color
	WHITE = "37"
)

// Clear console
func clearConsole() {

	var cmd *exec.Cmd
	if isWindows() {
		cmd = exec.Command("cmd", "/c", "cls")
	} else {
		cmd = exec.Command("clear")
	}

	cmd.Stdout = os.Stdout
	cmd.Run()
}

func checkError(e error) {
	if e != nil {
		dump(e)
		panic(nil)
	}
}

func dump(param interface{}) {
	fmt.Printf("%T", param)
	fmt.Printf(" (%+v)"+EOL, param)
}

func dumpr(param interface{}) string {
	s := ""
	s += fmt.Sprintf("%T", param)
	s += fmt.Sprintf(" (%+v)"+EOL, param)
	return s
}

// Exit program show error message
func exit(text string) {
	//clearConsole()
	pl("BŁĄD: "+text, RED)
	for {
		time.Sleep(30 * time.Second)
	}

}

func pl(arg ...string) {
	var text string
	if len(arg) == 1 {
		text = color(arg[0], WHITE)
	} else if len(arg) == 2 {
		text = color(arg[0], arg[1])
	} else {
		text = ""
	}
	fmt.Fprintln(console, " "+text)
}

func line(sign string) string {
	return strings.Repeat(sign, 98)
}

func color(text string, color string) string {
	return fmt.Sprintf("\x1b[%sm\x1b[1m%s\x1b[0m", color, text)
}

func input() string {
	text, _, _ := bufio.NewReader(os.Stdin).ReadLine()
	return string(text)
}

// Convert unix timestamp with format
func date(pattern string, unix int64) string {
	date := time.Unix(unix, 0)
	year, month, day := date.Date()
	hour, min, sec := date.Clock()

	f := map[string]string{}
	f["Y"] = strconv.Itoa(year)
	f["y"] = strconv.Itoa(year)[:2]
	f["j"] = strconv.Itoa(day)
	f["d"] = strconv.Itoa(day)
	f["n"] = fmt.Sprintf("%d", month)
	f["m"] = fmt.Sprintf("%d", month)
	f["G"] = strconv.Itoa(hour)
	f["H"] = strconv.Itoa(hour)
	f["i"] = strconv.Itoa(min)
	f["s"] = strconv.Itoa(sec)

	if len(f["d"]) == 1 {
		f["d"] = "0" + f["d"]
	}
	if len(f["m"]) == 1 {
		f["m"] = "0" + f["m"]
	}
	if len(f["H"]) == 1 {
		f["H"] = "0" + f["H"]
	}
	if len(f["i"]) == 1 {
		f["i"] = "0" + f["i"]
	}
	if len(f["s"]) == 1 {
		f["s"] = "0" + f["s"]
	}
	for symbol, value := range f {
		pattern = strings.Replace(pattern, symbol, value, -1)
	}
	return pattern
}

func logToFile(source string, cmd string) {

	if !config.Logging {
		return
	}

	date := date("d.m H:i:s", time.Now().Unix())
	line := fmt.Sprintf(EOL+"%s - %s - %s", date, source, cmd)

	old, err := ioutil.ReadFile(gitLogPath)
	if err == nil {
		line = string(old) + line
	}
	ioutil.WriteFile(gitLogPath, []byte(line), 0777)
}

func loggerAll(source string, cmd string, result string, nanosec int64) {
	// nie zapisujemy do logów zapytań o aktualną branche
	if strings.Contains(cmd, "rev-parse") {
		return
	}
	cmd = strings.Replace(cmd, "cmd /C", "", -1)
	var lg string
	lg += color(date("H:i:s", time.Now().Unix()), PINK) + color(" ["+source+"] ", CYAN) + color(cmd, WHITE)
	globalLog = append(globalLog, lg)
}
