package main

import (
	"fmt"
	"os"
	"runtime"
	"time"
)

const DEBUG_LOG = ".gogit_debug"

const LOGGING = true

func log(text string) {
	info := date("d.m H:i:s", time.Now().Unix())
	_, fn, line, _ := runtime.Caller(1)
	info += " " + fn + ":" + fmt.Sprintf("%d", line)
	text = info + TAB + text

	if LOGGING {
		f, err := os.OpenFile(DEBUG_LOG, os.O_CREATE|os.O_APPEND, 0777)
		if err != nil {
			dump(err)
			exit("problem z otwarciem pliku")
		}

		defer f.Close()

		if _, err = f.WriteString(EOL + text); err != nil {
			dump(err)
			exit("problem z zapisem do pliku")
		}
	}

}
