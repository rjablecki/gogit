package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func checkModules() []string {
	currentDir, _ = os.Getwd()
	var modules []string

	d, err := os.Open(currentDir)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer d.Close()
	fi, err := d.Readdir(-1)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	for _, fi := range fi {
		if fi.IsDir() {
			fi.Name()
			d, err := os.Open(currentDir + SEP + fi.Name() + "/.git")
			if err == nil {
				defer d.Close()
				modules = append(modules, fi.Name())
			}
		}
	}
	return modules
}

func globalLoop() {

	var err error

	printGlobalHeader()

	for {
		printGlobalMenu()
		cmd := input()
		printGlobalHeader()

		found := false
		for _, item := range globalMenu {
			if item.name == cmd {
				found = true
				item.action()
				break
			}
		}

		if !found {
			pl()
			pl("BŁĄD: nie ma takiej opcji", RED)
		}

		if currentModule != EMPTY_STRING {
			break
		}
		checkError(err)
	}

	moduleLoop()
}

func printGlobalMenu() {
	pl()
	pl()

	for _, item := range globalMenu {
		pl(color(item.name, YELLOW) + color(" ", WHITE) + color(item.label, CYAN) + " - " + color(item.description, WHITE))
	}

	pl()
	pl()
}

func checkStatusAll() bool {
	git := Git{}

	clear := true
	status := EMPTY_STRING
	pl(EOL+"Sprawdzanie repozytoriów:"+EOL, WHITE)

	for _, module := range modules {
		currentModule = module
		remoteNameLocal = git.getRemoteName(LOCAL)
		remoteDir = git.getRemotePath()
		isClear := git.isClear()
		if isClear {
			status = color("ok", GREEN)
		} else {
			status = color("zmiany", RED)
			clear = false
		}
		pl("- " + color(module, YELLOW) + ": " + status)
	}
	unsetModule()

	return clear
}

func synchroniseClear() bool {

	pl()

	for _, module := range modules {
		pl(color(module, YELLOW))
		currentModule = module
		remoteNameLocal = git.getRemoteName(LOCAL)
		remoteDir = git.getRemotePath()

		statusRes, _ := git.execute(REMOTE, "git status --porcelain")
		status, _ := parseStatusGIT(statusRes)

		if status.Clear == false {
			pl(" - " + color("są zmiany lecę dalej", RED))
			continue
		}

		pl(color(" - ", WHITE) + color("OK", GREEN) + color(" - na serwerze nie ma zmian sprawdzam branche i commity", WHITE))

		res, _ := git.execute(REMOTE, "git branch -v")
		remoteBranches := git.parseBranchesGIT(REMOTE, res)
		currentBranchRemote, currentRemoteHash = getCurrentBranch(remoteBranches)

		res, _ = git.execute(LOCAL, "git branch -va")
		localBranches := git.parseBranchesGIT(LOCAL, res)
		currentBranchLocal, currentLocalHash := getCurrentBranch(localBranches)

		update := false
		if currentBranchRemote != currentBranchLocal {
			pl(" - " + color("serwer: ", WHITE) + color(currentBranchRemote, CYAN) + color(" local: ", WHITE) + color(currentBranchLocal, CYAN) + color(" - aktualizujemy", GREEN))
			update = true
		} else {
			if currentRemoteHash != currentLocalHash {
				pl(" - " + color("serwer i local: ", WHITE) + color(currentBranchRemote, CYAN) + color(" ale róże commity. Local:", WHITE) + color(currentLocalHash, YELLOW) + color(" remote:", WHITE) + color(currentRemoteHash, YELLOW) + color(" - aktualizujemy", GREEN))
				update = true
			}
		}

		if update {
			git.checkoutLocal(currentBranchRemote)
		} else {
			pl(color(" - wszystko aktualne brancha: ", WHITE) + color(currentBranchRemote, CYAN) + color(" commit: ", WHITE) + color(currentLocalHash, YELLOW))
			git.execute(LOCAL, "git add . && git stash")
		}

	}

	pl(EOL + "skończone" + EOL)

	unsetModule()

	return true
}

func synchroniseAll() {
	clear := checkStatusAll()
	currentModule = EMPTY_STRING

	if clear == false {
		pl(EOL+"BŁĄD: Masz nieskomitowane zmiany", RED)
		return
	}

	if clear {
		pl("")
		pl(EOL+"Mergowanie:"+EOL, WHITE)
		for _, module := range modules {
			pl("- " + color(module, YELLOW))

			currentModule = module
			remoteNameLocal = git.getRemoteName(LOCAL)
			remoteDir = git.getRemotePath()

			pl(color(" gitlab", GREEN) + color(" => ", RED) + color("remote", GREEN))

			git.execute(REMOTE, "git fetch gitlab")
			git.execute(REMOTE, "git merge gitlab/master")

			res, _ := git.execute(REMOTE, "git branch -v")
			remoteBranches := git.parseBranchesGIT(REMOTE, res)
			currentBranchRemote, currentRemoteHash = getCurrentBranch(remoteBranches)

			pl(color(" remote", GREEN) + color(" => ", RED) + color("local", GREEN))
			res, _ = git.execute(LOCAL, "git fetch -p "+remoteNameLocal)
			res, _ = git.execute(LOCAL, "git branch -va")

			git.execute(LOCAL, "git add . && git stash")
			git.execute(LOCAL, "git branch -D "+currentBranchRemote)
			git.execute(LOCAL, "git checkout -b "+currentBranchRemote+" "+remoteNameLocal+"/"+currentBranchRemote)

		}
		currentModule = ""
	}
}

func addRepo() bool {

	if len(config.ServerHost) == 0 {
		pl("BŁĄD: Nie ma zdefiniowanego serwera. Przejdź do ustawień i wybierz", RED)
		return false
	}

	pl(color(EOL+" Podaj absolutną ścieżkę do repozytorium na serwerze: (/var/www/vhosts/project/user_html/repo/.git)"+EOL, WHITE))
	remotePath := input()
	if len(remotePath) == 0 {
		pl("BŁĄD: Ścieżka jest obowiązkowa", RED)
		return false
	}
	pathMatchPattern, _ := regexp.MatchString("^[a-zA-Z0-9_/.]+\\.git$", remotePath)
	if !pathMatchPattern {
		pl("BŁĄD: Ścieżka jest nieprawidłowa ^[a-zA-Z0-9_/.]+\\.git$", RED)
		return false
	}

	output, err := runCmd("cd " + remotePath + " && ls -lh")
	if len(output) == 0 {
		pl("BŁĄD: Nie ma takiego folderu na serwerze", RED)
		return false
	}

	path := strings.Replace(remotePath, "/.git", "", -1)
	parts := strings.Split(path, SEP)
	repoName := parts[len(parts)-1]
	localRepoPath := currentDir + SEP + repoName

	d, err := os.Open(localRepoPath)
	if err == nil {
		defer d.Close()
		pl("BŁĄD: Takie repo już istnieje na localu", RED)
		return false
	}

	remoteName := "srv"
	os.Mkdir(localRepoPath, 0777)

	execa("git init", localRepoPath)
	command := fmt.Sprintf("git remote add %s ssh://%s%s", remoteName, config.ServerHost, remotePath)
	execa(command, localRepoPath)

	modules = checkModules()
	currentModule = repoName

	git := Git{}

	git.CheckoutLocalRepoLikeServer()
	return true
}

func selectRepo() bool {
	if len(modules) == 0 {
		pl("BŁĄD: nie ma co wybierać", RED)
		return false
	}

	pl(color(EOL+" Wybierz repozytorium:"+EOL, WHITE))
	for key, module := range modules {
		pl(color(fmt.Sprintf("%d", key+1), YELLOW) + color(" - ", WHITE) + color(module, CYAN))
	}
	number, _ := strconv.Atoi(input())
	if number < 1 || number > len(modules) {
		pl("BŁĄD: nieprawidłowy wybór", RED)
		return false
	}
	currentModule = modules[number-1]

	return true
}

func printGlobalHeader() {
	clearConsole()

	header := EOL + " GoGit"

	//colors := []string{RED,GREEN,YELLOW,BLUE,PINK,CYAN,WHITE}
	//for _,c := range colors {
	//	header += " "+color("test", c)
	//}

	header += EOL + color(line("_")+"__", WHITE)
	pl(header)
}
