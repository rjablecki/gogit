# GOGIT #

To samodzielna aplikacja napisana w języku Go. Służy do jednoczesnego zarządzania lokalnym i zdalnym repozytorium GIT. Posiada kolorowanie składni, co ułatwia pracę z dużą ilością tekstu w konsoli. Pozwala na zaoszczędzenie czasu np. jednoczesna zmiana gałęzi na lokalu i serwerze jest uruchamiana jednym przyciskiem włącznie z synchronizacją plików lokalnych.

### Możliwości ###

* Listowanie aktualnych zmian w plikach
* Wyświetlanie listy gałęzi
* Commitowanie zmian
* Zmiana branchy
* Tworzenie nowej branchy
* Mergowanie 
* Wyświetlenie wszystkich commitów
* Połączenie z serwerem przez SSH, logwanie hasłem lub kluczem prywatnym

### Jak zacząć ###

* Skopiować gogit.exe do folder z projektem
* Projekt musi mieć zainicjalizowane repozytorium GIT
* Git musi mieć ustawione zdalne repozytorium
* Jeśli do połączenia z serwerem zewnętrznym wykorzystujemy klucz to musi być bez hasła
* Klucz musi znajdować się w folderze ~/.ssh/

#### Widok menu: ####

![menu.png](https://bitbucket.org/repo/k6BMaa/images/1656507585-menu.png)

#### Widok listy gałęzi: ####

![branch.png](https://bitbucket.org/repo/k6BMaa/images/1751582382-branch.png)

#### Widok listy commitów: ####

![log.png](https://bitbucket.org/repo/k6BMaa/images/3413229448-log.png)