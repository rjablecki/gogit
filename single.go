package main

import (
	"strings"
	"time"
)

// konstruktor aplikacji
// zawiera nieskączoną pętlę w której sprawdzamy jaka opcja z menu została wybrana
// jeśli wybrano nieprawidłową opcję to powtarzamy pytanie
func moduleLoop() {
	var res string
	var err error
	first := true
	git := Git{}

	remoteNameLocal = git.getRemoteName(LOCAL)
	remoteDir = git.getRemotePath()

	updateRemoteBranch()

	clearConsole()
	for {
		res, err = git.execute(LOCAL, "git branch -va")
		localBranches := git.parseBranchesGIT(LOCAL, res)
		currentBranchLocal, currentLocalHash = getCurrentBranch(localBranches)

		if first {
			printHeader()
		}
		printMenu()
		cmd := input()
		clearConsole()
		printHeader()

		found := false
		for _, item := range singleMenu {
			if item.name == cmd {
				found = true
				item.action()
				break
			}
		}

		if !found {
			pl()
			pl("BŁĄD: nie ma takiej opcji", RED)
		}

		if currentModule == "" {
			break
		}

		checkError(err)
		first = false
	}

	globalLoop()
}

func unsetModule() {
	currentModule = ""
}

// wyświetlenie menu
func printMenu() {

	remoteUrlLocal, _ := git.execute(LOCAL, "git remote get-url "+remoteNameLocal)

	pl()
	pl(color("Remote: "+remoteNameLocal+" "+remoteUrlLocal, CYAN))
	pl()

	for _, item := range singleMenu {
		pl(color(item.name, YELLOW) + color(" ", WHITE) + color(item.label, WHITE))
	}

}

// wyświetlenie nagłówka
// - nazwa projektu
// - aktualna brancha lokalna
// - aktualna brancha zdalna
func printHeader() {
	cbl := currentBranchLocal

	if len(cbl) > 10 {
		cbl = cbl[:10] + "..."
	}
	cbr := currentBranchRemote

	if len(cbr) > 10 {
		cbr = cbr[:10] + "..."
	}

	header := EOL
	header += color("  repozytorium: ", WHITE) + color(currentModule, YELLOW) + TAB
	header += color("  local: ", WHITE) + color(cbl, GREEN) + " " + color(currentLocalHash, YELLOW) + TAB
	header += color("serwer: ", WHITE) + color(cbr, GREEN) + " " + color(currentRemoteHash, YELLOW)
	header += EOL + color(line("_")+"__", WHITE)
	pl(header)
}

func updateRemoteBranch() {
	res, err := git.execute(REMOTE, "git rev-parse --abbrev-ref HEAD && git log -n 1 --pretty=tformat:%H")
	if err != nil {
		return
	}
	lines := strings.Split(res, EOL)
	var name string
	for i, line := range lines {
		if i == 0 {
			name = line
		}
		if i == 1 {
			if len(line) > 3 {
				currentRemoteHash = line[:3]
			} else {
				currentRemoteHash = line
			}
		}
	}
	if currentBranchRemote != "" && name != currentBranchRemote {
		pl(color(date("H:i:s", time.Now().Unix())+" - na serwerze zmieniono branche na: ", RED) + color(name, GREEN))
	}
	currentBranchRemote = name
}
