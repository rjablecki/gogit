package main

import "strings"

const (
	EOL          = "\n"
	TAB          = "\t"
	EMPTY_STRING = ""
	SEP          = "/"
)

func normalizeOutput(output string) []string {
	var final []string
	lines := strings.Split(output, EOL)
	for _, rawLine := range lines {
		line := strings.TrimSpace(rawLine)
		if len(line) == 0 {
			continue
		}
		final = append(final, line)
	}
	return final
}

// Show git status
// Du - deleted
// Mu - modified
// Nu - new
// A - new
// D - deleted
// M - modified
// N - xxx
func showStatusGIT(res string) {
	s, _ := parseStatusGIT(res)
	if s.Clear == true {
		pl(EOL+TAB+TAB+TAB+TAB+"BRAK ZMIAN - NIC MA CO COMMITOWAĆ"+EOL, YELLOW)
		return
	}
	if len(s.A) > 0 || len(s.D) > 0 || len(s.M) > 0 || len(s.N) > 0 {
		pl(color("DODANE DO COMMITA:", YELLOW) + TAB + color("nowy ", BLUE) + color("zmodyfikowany ", CYAN) + color("usunięty ", RED) + EOL)
		if len(s.A) > 0 {
			for _, el := range s.A {
				pl(" - "+el, BLUE)
			}
		}
		if len(s.D) > 0 {
			for _, el := range s.D {
				pl(" - "+el, RED)
			}
		}
		if len(s.M) > 0 {
			for _, el := range s.M {
				pl(" - "+el, CYAN)
			}
		}
		if len(s.N) > 0 {
			for _, el := range s.N {
				pl(" - "+el, PINK)
			}
		}
	}
	if len(s.Du) > 0 || len(s.Mu) > 0 || len(s.Nu) > 0 {
		pl(color("NIEDODANE DO COMMITA:", YELLOW) + TAB + color("nowy ", BLUE) + color("zmodyfikowany ", CYAN) + color("usunięty ", RED) + EOL)
		if len(s.Du) > 0 {
			for _, el := range s.Nu {
				pl(" - "+el, RED)
			}
		}
		if len(s.Mu) > 0 {
			for _, el := range s.Mu {
				pl(" - "+el, CYAN)
			}
		}
		if len(s.Nu) > 0 {
			for _, el := range s.Nu {
				pl(" - "+el, BLUE)
			}
		}
	}
}

// Parse git status string
func parseStatusGIT(result string) (*gitStatusResponse, error) {
	status := new(gitStatusResponse)
	status.M = []string{}
	status.Mu = []string{}
	status.N = []string{}
	status.Nu = []string{}
	status.D = []string{}
	status.Du = []string{}
	status.A = []string{}

	if len(result) == 0 {
		status.Ready = false
		status.Clear = true
	} else {
		status.Clear = false
		lines := strings.Split(result, EOL)

		for _, line := range lines {
			trimed := strings.TrimSpace(line)
			if len(trimed) == 0 {
				continue
			}
			prefix := line[:2]
			file := line[2:]
			file = strings.TrimSpace(file)
			switch prefix {
			case "??":
				status.Nu = append(status.Nu, file)
				break
			case "AM":
				status.Nu = append(status.Nu, file)
				break
			case "M ":
				status.M = append(status.M, file)
				break
			case "MM":
				status.Mu = append(status.Mu, file)
				break
			case " M":
				status.Mu = append(status.Mu, file)
				break
			case "D ":
				status.D = append(status.D, file)
				break
			case " D":
				status.Du = append(status.Du, file)
				break
			case "A ":
				status.A = append(status.A, file)
				break
			default:
				dump("nie weszlismy w switch. prefix:" + prefix)
			}
		}
		if len(status.Nu) > 0 || len(status.Mu) > 0 || len(status.Du) > 0 {
			status.Ready = false
		} else {
			status.Ready = true
		}
	}
	return status, nil
}
